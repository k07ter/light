#coding: utf-8

from django.db import models
from django.apps import AppConfig
from es import settings

ISSUE_STATS = (
	(1, u'Одобрено'),
	(0, u'Отказано'),
)

PAYMENT_PERIOD = (
    ('W', u'Неделя'),
    ('M', u'Месяц'),
    ('3M', u'3 месяца'),
)
PAYMENT_STATUS = (
    (1, u'Оплачено'),
    (0, u'Не оплачено'),
)

# Create your models here.
class Person(models.Model):
	class Meta:
		abstract = True

	fam = models.CharField(u'Фамилия:', max_length=20)
	name = models.CharField(u'Имя:', max_length=15)
	otch = models.CharField(u'Отчество:', max_length=30)
	#att_nom = models.CharField(u'Номер аттестата', max_length=10)
	email = models.EmailField('email:', max_length=20)
	#documenty = models.FileField(u'Документы', upload_to=settings.UPLOADS_ROOT + 'docs/', blank=True)
	user_status = models.CharField(u'Статус пользователя', choices = (('z', u'Заказчик'),('i', u'Исполнитель')), blank=True, max_length=1)
	login = models.CharField(u'Логин:', max_length=10)
	parol = models.CharField(u'Пароль:', max_length=10)
	payment_period_type = models.CharField(u'Период оплаты:', choices=PAYMENT_PERIOD, default='W', max_length=3)
	payment_status = models.IntegerField(u'Статус оплаты:', choices=PAYMENT_STATUS, default=0)
	
#	def load_data(self, db, source):
#	    '''Импорт данных в базу из внешнего источника'''
#	    #cust_db = Customer.objects.use('customer').query("select * from customers")
#	    any_db = '' #db.objects.use('customer').query("select * from customers")
#	    #print any_db.all()
#
#	def dump_data(self, db, dest):
#	    '''Выгрузка данных'''
#	    cust_backup = ''

class Sprkind(models.Model):
	name = models.CharField(u'Наименование', max_length=40)

	class Meta:
	    db_table = 'sprkinds'
	    verbose_name = u'Сфера деятельности'
	    verbose_name_plural = u'Сферы деятельности'

	def __str__(self):
	    return u'%s' % self.name

#def SprkindConfig(AppConfig):
#	name = u'Справочники СД'

class Sprspec(models.Model):
	kind = models.ForeignKey(Sprkind, verbose_name=u'Сфера деятельности:')
	nazv = models.CharField(u'Специализация:', max_length=30)
	
	class Meta:
	    db_table = 'sprspecs'
	    verbose_name = u'Специализация'
	    verbose_name_plural = u'Специализации'
	
	def __str__(self):
	    return u'%s' % self.nazv

class Sprorg(models.Model):
	fullname = models.CharField(u'Полное наименование', max_length=255)
	shortname = models.CharField(u'Краткое наименование', max_length=100)
	adress_ur = models.CharField(u'Юридический адрес', max_length=255)
	phone_ur = models.CharField(u'Телефон организации:', max_length=10)
	email_ur = models.EmailField(u'Электронная почта:')
	ogrn = models.CharField(u'ОГРН:', max_length=15)
	inn = models.CharField(u'ИНН:', max_length=14)
	kind_ur = models.ForeignKey(Sprkind, verbose_name=u'Сфера деятельности:')

	def __str__(self):
	    return u'%s' % self.shortname

	class Meta:
	    db_table = 'sprorgs'
	    verbose_name = u'Организация'
	    verbose_name_plural = u'Организации'

#def ManagerConfig(AppConfig):
#    name = 'es'
#    verbose_name = u'Ядро и справочники'


'''
class DomainMiddleware(object):
    """Gets the correct instance of an application-specific model by matching the
    sub-domain of the request."""

    def __init__(self):
        self.site_domain = Site.objects.get_current().domain
        if self.site_domain.startswith('www.'):
            self.site_domain = self.site_domain[4:]
        self.SUBDOMAIN_RE = re.compile(r'^(?:www\.)?(?P<slug>[\w-]+)\.%s' % re.escape(self.site_domain))
        try:
            app_name, model_name = settings.DOMAIN_MIDDLEWARE_MODEL.split('.', 2)
            self.model = get_model(app_name, model_name)
            self.instance_name = settings.DOMAIN_MIDDLEWARE_INSTANCE_NAME
            assert self.instance_name
        except (AttributeError, AssertionError):
            raise ImproperlyConfigured('DomainMiddleware requires DOMAIN_MIDDLEWARE_MODEL and DOMAIN_MIDDLEWARE_INSTANCE_NAME settings')

    def process_view(self, request, view_func, view_args, view_kwargs):
        """If domain is not main site, check for subdomain.

        Get the model from the subdomain slug.
        """
        port = request.META.get('SERVER_PORT')
        domain = request.META.get('HTTP_HOST', '').replace(':%s' % port, '')
        if domain.startswith('www.'):
            domain = domain[4:]
        if domain != self.site_domain:
            match = self.SUBDOMAIN_RE.match(domain)
            if match:
                slug = match.group('slug')
                instance = get_object_or_404(self.model, slug=slug)
            setattr(request, self.instance_name, instance)
        return None

    def index(request):
        members = request.club.members.approved().order_by('-creation_date')
        return list_detail.object_list(
            request                 = request,
            queryset                = members,
            template_name           = 'ecpd/index.html',
            template_object_name    = 'member'
        )
'''

#class Issue(models.Model):
#	issue_nom = models.IntegerField(u'Номер заявки')
#	customer_id = models.ForeignKey(u'Заказчик', Customer)
#       performer_id = models.ForeignKey(u'Исполнитель', Performer)
#	status = models.BooleanField(u'Решение по МП', choices=ISSUE_STATS)
	#date = models.DateField(u'Дата решения')

#class Service():
#	name = models.CharField(u'Наименование', max_length=10)

#class DBRouter(object):
#	def db_for_read(self, model, **kwargs):
#		return model._meta.model_name
#
#	def db_for_write(self, model, **kwargs):
#		return model._meta.model_name
#
#	def db_for_delete(self, model, **kwargs):
#		return model._meta.model_name
