#coding: utf-8
from django.shortcuts import render, render_to_response
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from performer.models import Performer
from django import forms
import json

#from es.sitemap import front, lk
# Create your views here.
#def page_out(request, *args, **kwarg):
#        print(args)
#        print(kwarg)
#        return render_to_response(args, kwarg, context_instance=RequestContext(request))

"""
class RegForm(forms.Form):
        login = forms.CharField(u'Логин:', max_length=10)
        parol = forms.CharField(u'Пароль:', widget=forms.PasswordInput)
        parol2 = forms.CharField(u'Пароль (повторно):', widget=forms.PasswordInput)

        def register(self, request):
            if request.method == 'POST':
                return None
            else:
                form = RegForm()
            return render(request, '', {'form': form })

class LoginForm(forms.Form):
        login = forms.CharField(u'Логин:', max_length=10)
        parol = forms.CharField(u'Пароль:', widget=forms.PasswordInput())

        def login(self, request):
            if request.METHOD == 'POST':
                login = request.POST['login']
                parol = request.POST['parol']

        def get_user(self):
            return self.user or None
items = [
        {'name': u'Услуга', 'age': 15, 'lnk': '/uslugi' },
        {'name': u'Публикации', 'age': 12, 'lnk': '/publ' },
        {'name': u'Курсы и обучение', 'age': 25, 'lnk': '/kursy' },
        {'name': u'Отзывы', 'age': 12, 'lnk': '/fb' },
        {'name': u'Информация', 'age': 35, 'lnk': '/info' }
        ]
"""

def home(request):
        '''
        top_items = [
        	 {'name': u'Работа', 'age': 12, 'lnk': '/t_job' },
        	 {'name': u'Услуги', 'age': 25, 'lnk': '/t_usl' },
        	 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_ku' },
        	 {'name': u'Форум', 'age': 35, 'lnk': '/t_forum' },
        	 {'name': u'Исполнители', 'age': 27, 'lnk': '/t_isp' },
        ]'''
        #return page_out(request, )
        from es.sitemap import front
        #from sm import sm.__all__
        top_items = front.menu
        #print(top_items)
        #json.load(open('es/sitemap/front/menu.json'))
        return render_to_response('ecpd/index.html',
                                    {'data': None, 'items': top_items },
                                    context_instance=RequestContext(request))

def lk(request, arg=None):
        #print(arg)
        from es.sitemap import front, lk
        """
        #get = request.GET
        #print(request.method)
        items = [
        {'name': u'Услуга', 'age': 15, 'lnk': 'uslugi' },
        {'name': u'Публикации', 'age': 12, 'lnk': 'publ' },
        {'name': u'Курсы и обучение', 'age': 25, 'lnk': 'kursy' },
        {'name': u'Отзывы', 'age': 12, 'lnk': 'fb' },
        {'name': u'Информация', 'age': 35, 'lnk': 'info' }
        ]

        item_ = [
        {'name': u'Услуга', 'age': 15, 'lnk': '/servis' },
        {'name': u'Публикации', 'age': 12, 'lnk': '/publ' },
        {'name': u'Курсы и обучение', 'age': 25, 'lnk': '/uslugi' },
        {'name': u'Отзывы', 'age': 12, 'lnk': '/fb' },
        {'name': u'Информация', 'age': 35 }
        ]
        """

        account = {}
        topitems = [
        	 {'name': u'Работа', 'age': 12, 'lnk': '/t_job' },
        	 {'name': u'Услуги', 'age': 25, 'lnk': '/t_usl' },
        	 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_ku' },
        	 {'name': u'Форум', 'age': 35, 'lnk': '/t_forum' },
        	 {'name': u'Исполнители', 'age': 27, 'lnk': '/t_isp' },
        ]

        #print(dir(fr))
        items = front.menu
        #print(top_items)
        top_items = lk.menu
        #print(request.META)

        ku_items = [
        {'name': u'курс1', 'cost': 12, 'lnk': '/krs1' },
        {'name': u'курс2', 'cost': 25, 'lnk': '/krs2' },
        {'name': u'курс3', 'cost': 15, 'lnk': '/krs3' },
        {'name': u'курс4', 'cost': 13, 'lnk': '/krs4' },
        ]

        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 1.5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
            'kursy': range(1,3),
                }


        if request.method == 'POST':
            post = request.POST
            #req = post or get
            #print(get)
            #print(post)
            #form = LoginForm(request.POST)

            #if form.is_valid():
            #    if form.get_user():
            #        login(request, form.get_user())
            #        return HttpResponseRedirect('/')
            #else:
            #form = LoginForm()

            login = post.get('login')
            parol = post.get('passw')
            #email = post.get('email')
            #print(login)
            #print(req.__dict__)
            #account = Performer.objects.filter(login=login).query(['fam', 'name', 'otch', 'email'])
            try:
                account = Performer.objects.get(login=login)

                #print(account.parol)
                if login and parol == account.parol:
                    user_props.update({'account': account, 'self': True})
                    #print(email)
                    #import es.sitemap as sm
                    print(user_props)
                    return render_to_response('ecpd/lk_cust.html', user_props, context_instance=RequestContext(request))
                else:
                    return render_to_response('performer/add_res.html',
                                                {'res': 'Ошибка. Неверный логин и/или пароль', 'lnk': 'lk', 'lnk_text': u'Повторить' })
                                                 #, {'items': items, 'top_items': top_items , 'account': account }, context_instance=RequestContext(request))
            except:
                return render_to_response('performer/add_res.html',
                                            {'res': 'Ошибка. Неверный логин и/или пароль', 'lnk': 'lk', 'lnk_text': u'Повторить' })
                #, {'items': items, 'top_items': top_items , 'account': account }, context_instance=RequestContext(request))
                #return HttpResponse('qweqwe') #request, 'ecpd/lk_cust.html', {'items': items, 'top_items': top_items , 'account': account }) #, context_instance=RequestContext(request))
                #return HttpResponseRedirect('/')
        else:
            cook = request.COOKIES
            #print(login)
            if arg:
                account = Performer.objects.get(login=arg)
                if account and cook.get('login') == arg:
                    user_props.update({'self': True})
                return render_to_response('ecpd/lk_cust.html', user_props,
                                            context_instance=RequestContext(request))
            else:
                return render_to_response('ecpd/login.html', user_props,
                                            context_instance=RequestContext(request))

def webinary(request, arg=None):
        from es.sitemap import front, lk

        #items = front.menu
        #top_items = lk.menu

        tems = [
        {'name': u'Услуга', 'age': 15, 'lnk': '/uslugi' },
        {'name': u'Публикации', 'age': 12, 'lnk': '/publ' },
        {'name': u'Курсы и обучение', 'age': 25, 'lnk': '/kursy' },
        {'name': u'Отзывы', 'age': 12, 'lnk': '/fb' },
        {'name': u'Информация', 'age': 35, 'lnk': '/info' }
        ]
        item = [
        {'name': u'Работа', 'age': 12, 'lnk': '/job' },
        {'name': u'Сервис', 'age': 25, 'lnk': '/servis' },
        {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' },
        {'name': u'Информация', 'age': 35, 'lnk': '/info' }
        ]
        ku_items = [
        {'name': u'курс1', 'cost': 12, 'lnk': '/krs1' },
        {'name': u'курс2', 'cost': 25, 'lnk': '/krs2' },
        {'name': u'курс3', 'cost': 15, 'lnk': '/krs3' },
        {'name': u'курс4', 'cost': 13, 'lnk': '/krs4' },
        ]

        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 1.5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
            'kursy': range(1,3),
        }

        return render_to_response('ecpd/lk_webinary.html', user_props,
                                    context_instance=RequestContext(request))

def job(request, arg=None):
        from es.sitemap import front, lk
        account = request.POST
        #item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]
        items = front.menu
        top_items = lk.menu
        ku_items = [
        {'name': u'курс1', 'cost': 12, 'lnk': '/krs1' },
        {'name': u'курс2', 'cost': 25, 'lnk': '/krs2' },
        {'name': u'курс3', 'cost': 15, 'lnk': '/krs3' },
        {'name': u'курс4', 'cost': 13, 'lnk': '/krs4' },
        ]

        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 1.5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
            'kursy': range(1,3),
            'account': account,
        }

        return render_to_response('ecpd/lk_job.html', user_props,
                                    context_instance=RequestContext(request))

def uslugi(request, arg=None):
        account = request.POST
        from es.sitemap import front, lk
        items = front.menu
        top_items = lk.menu
        """
        items = [
        {'name': u'Услуга', 'age': 15, 'lnk': '/uslugi' },
        {'name': u'Публикации', 'age': 12, 'lnk': '/publ' },
        {'name': u'Курсы и обучение', 'age': 25, 'lnk': '/kursy' },
        {'name': u'Отзывы', 'age': 12, 'lnk': '/fb' },
        {'name': u'Информация', 'age': 35, 'lnk': '/info' }
        ]
        item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]
        """
        ku_items = [
        {'name': u'курс1', 'cost': 12, 'lnk': '/krs1' },
        {'name': u'курс2', 'cost': 25, 'lnk': '/krs2' },
        {'name': u'курс3', 'cost': 15, 'lnk': '/krs3' },
        {'name': u'курс4', 'cost': 13, 'lnk': '/krs4' },
        ]

        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 1.5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
            'kursy': range(1,3),
            'account': account,
        }


        return render_to_response('ecpd/lk_uslugi.html', user_props,
                                    context_instance=RequestContext(request))

def fb(request, arg=None):
        items = [
        {'name': u'Услуга', 'age': 15, 'lnk': '/uslugi' },
        {'name': u'Публикации', 'age': 12, 'lnk': '/publ' },
        {'name': u'Курсы и обучение', 'age': 25, 'lnk': '/kursy' },
        {'name': u'Отзывы', 'age': 12, 'lnk': '/fb' },
        {'name': u'Информация', 'age': 35, 'lnk': '/info' }
        ]
        item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]
        from es.sitemap import front, lk
        items = front.menu
        top_items = lk.menu

        return render_to_response('ecpd/lk_fb.html',
                                    {'items': items, 'top_items': top_items, 'account': {'name': 'none'}},
                                    context_instance=RequestContext(request))

def publ(request, arg=None):
        items = [
        {'name': u'Услуга', 'age': 15, 'lnk': '/uslugi', 'text': 'qewwqewqe wqe wqe qwe qwe wqe qwe wq e'},
        {'name': u'Публикации', 'age': 12, 'lnk': '/publ', 'text': 'fdfdsfcdscdscfdscfdsfdsfcdsdcfds fdscfds' },
        {'name': u'Курсы и обучение', 'age': 25, 'lnk': '/kursy', 'text': 'sff' },
        {'name': u'Отзывы', 'age': 12, 'lnk': '/fb', 'text': 'tertre' },
        {'name': u'Информация', 'age': 35, 'lnk': '/info', 'text': '' }
        ]
        item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]
        from es.sitemap import front, lk
        items = front.menu
        top_items = lk.menu

        return render_to_response('ecpd/lk_publ.html',
                                    {'items': items, 'top_items': top_items},
                                    context_instance=RequestContext(request))

def info(request, arg=None):
        items = [
        {'name': u'Услуга', 'age': 15, 'lnk': '/uslugi' },
        {'name': u'Публикации', 'age': 12, 'lnk': '/publ' },
        {'name': u'Курсы и обучение', 'age': 25, 'lnk': '/kursy' },
        {'name': u'Отзывы', 'age': 12, 'lnk': '/fb' },
        {'name': u'Информация', 'age': 35, 'lnk': '/info' }
        ]
        item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]
        from es.sitemap import front, lk
        items = front.menu
        top_items = lk.menu

        return render_to_response('ecpd/lk_info.html',
                                    {'items': items, 'top_items': top_items },
                                    context_instance=RequestContext(request))

def reg(request): #, success_url=None, template_name='ecpd/reg_form.html', extra_context=None):
        item = {}
        return render_to_response('ecpd/reg.html', {'items': items})

def reg_usr(request): #, success_url=None, template_name='ecpd/reg_form.html', extra_context=None):
        item = {}
        return render_to_response('ecpd/reg_usr.html', {'items': items})


def t_job(request, arg=None):
        from es.sitemap import front, lk
        """
        top_items = [{'name': u'Работа', 'age': 12, 'lnk': 't_job' },
    		 {'name': u'Услуги', 'age': 25, 'lnk': 't_usl' },
    		 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_ku' },
    		 {'name': 'Форум', 'age': 35, 'lnk': '/t_forum' },
    		 {'name': 'Исполнители', 'age': 27, 'lnk': '/t_isp' },
    		 ]
    	"""
    	
        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Задача на базы', 'content': u'Заказ выполнен по разработке баз' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение о конвертации', 'content': u'Предлагаю работу по конвертации данных' , 'age': 3, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение связи', 'content': u'Возможность групповой работы в сетях' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            {'tag': u'Обсуждение коллизий', 'content': u'Форвард в сетях' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
        }
        return render_to_response('ecpd/job_list.html', user_props)

def t_forum(request, arg=None):
        from es.sitemap import front, lk
        items = front.menu
        top_items = lk.menu

        topitems = [
    		 {'name': u'Работа', 'age': 12, 'lnk': '/t_job' },
    		 {'name': u'Услуги', 'age': 25, 'lnk': '/t_usl' },
    		 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_w' },
    		 {'name': 'Форум', 'age': 35, 'lnk': '/t_forum' },
    		 {'name': 'Исполнители', 'age': 27, 'lnk': '/t_isp' },
    		 ]

        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 1.5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
        }

        return render_to_response('ecpd/forum.html', user_props
                                    #{'top_items': top_items, 'items': items}
                                    )

def t_isp(request, arg=None):
        from es.sitemap import front, lk
        items = front.menu
        top_items = lk.menu

        topitems = [{'name': u'Работа', 'age': 12, 'lnk': '/t_job' },
    		 {'name': u'Услуги', 'age': 25, 'lnk': '/t_usl' },
    		 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_ku' },
    		 {'name': 'Форум', 'age': 35, 'lnk': '/t_forum' },
    		 {'name': 'Исполнители', 'age': 27, 'lnk': '/t_isp' },
    		 ]

        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 1.5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
        }

        return render_to_response('ecpd/vse_isp.html', user_props
                                    #{'top_items': top_items, 'items': items}
                                    )

def t_usl(request, arg=None):
        from es.sitemap import front, lk
        items = front.menu
        top_items = lk.menu

        topitems = [{'name': u'Работа', 'age': 12, 'lnk': '/t_job' },
    		 {'name': u'Услуги', 'age': 25, 'lnk': '/t_usl' },
    		 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_ku' },
    		 {'name': 'Форум', 'age': 35, 'lnk': '/t_forum' },
    		 {'name': 'Исполнители', 'age': 27, 'lnk': '/t_isp' },
    		 ]
        item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]

        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 1.5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
        }
        return render_to_response('ecpd/usl.html', user_props
                                    #{'top_items': top_items, 'items': items}
                                    )

def t_webinary(request, arg=None):
        from es.sitemap import front, lk
        """
        top_items = [{'name': u'Работа', 'age': 12, 'lnk': '/t_job' },
    		 {'name': u'Услуги', 'age': 25, 'lnk': '/t_usl' },
    		 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_ku' },
    		 {'name': 'Форум', 'age': 35, 'lnk': '/t_forum' },
    		 {'name': 'Исполнители', 'age': 27, 'lnk': '/t_isp' },
    		 ]
        item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]
        """
        items = front.menu
        top_items = lk.menu
        ku_items = [
        {'name': u'курс1', 'cost': 12, 'lnk': '/krs1' },
        {'name': u'курс2', 'cost': 25, 'lnk': '/krs2' },
        {'name': u'курс3', 'cost': 15, 'lnk': '/krs3' },
        {'name': u'курс4', 'cost': 13, 'lnk': '/krs4' },
        ]

        partners = [{'name': u'Иван', 'age': 2, 'lnk': '/iv1' }, {'name': u'Вася', 'age': 5, 'lnk': '/vasya' }]
        items = front.menu
        top_items = lk.menu
        
        rss_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]

        tsk_items = [
            {'tag': u'Отзыв', 'content': u'Заказ выполнен вовремя' , 'age': 2, 'lnk': '/iv1', 'cost': 1200 },
            {'tag': u'Преложение', 'content': u'Предлагаю работу' , 'age': 1.5, 'lnk': '/vas', 'cost': 1500 },
            {'tag': u'Обсуждение', 'content': u'Возможность групповой работы' , 'age': 3, 'lnk': '/greg', 'cost': 2300 },
            ]
        
        user_props = {
            'items': items,
            'top_items': top_items,
            'rss_items': rss_items,
            'tsk_items': tsk_items,
            'partners': partners,
            'ku_items': ku_items,
        }

        return render_to_response('ecpd/webinary.html', user_props
                                    #{'top_items': top_items, 'items': items, 'ku_items': ku_items}
                                    )

#@csrf_protect
def r_f(request):
        from es.sitemap import front, lk
        """
        #form = RegForm()
        top_items = [
    		 {'name': u'Работа', 'age': 12, 'lnk': '/t_job' },
    		 {'name': u'Услуги', 'age': 25, 'lnk': '/t_usl' },
    		 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_ku' },
    		 {'name': 'Форум', 'age': 35, 'lnk': '/t_forum' },
    		 {'name': 'Исполнители', 'age': 27, 'lnk': '/t_isp' },
    		 ]
        item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]
        """
        items = front.menu
        top_items = lk.menu
        ku_items = [
        {'name': u'курс1', 'cost': 12, 'lnk': '/krs1' },
        {'name': u'курс2', 'cost': 25, 'lnk': '/krs2' },
        {'name': u'курс3', 'cost': 15, 'lnk': '/krs3' },
        {'name': u'курс4', 'cost': 13, 'lnk': '/krs4' },
        ]
        account = {}
        return render_to_response('ecpd/r_f.html',
                                    {'top_items': top_items, 'items': items, 'ku_items': ku_items, 'account': account, 'form': ['form'] },
                                    context_instance=RequestContext(request))

def issue(request):
        return render_to_response('issue/index.html') #, {'top_items': top_items, 'items': items, 'ku_items': ku_items})

def call(request):
        from es.sitemap import front, lk
        """
        top_items = [
    		 {'name': u'Работа', 'age': 12, 'lnk': '/t_job' },
    		 {'name': u'Услуги', 'age': 25, 'lnk': '/t_usl' },
    		 {'name': u'Курсы и обучение', 'age': 15, 'lnk': '/t_ku' },
    		 {'name': 'Форум', 'age': 35, 'lnk': '/t_forum' },
    		 {'name': 'Исполнители', 'age': 27, 'lnk': '/t_isp' },
    		 ]
        item = [{'name': u'Работа', 'age': 12, 'lnk': '/job' }, {'name': u'Сервис', 'age': 25, 'lnk': '/servis' }, {'name': u'Услуги', 'age': 15, 'lnk': '/uslugi' }, {'name': 'lucya', 'age': 35 }]
        """
        items = front.menu
        top_items = lk.menu
        ku_items = [
        {'name': u'курс1', 'cost': 12, 'lnk': '/krs1' },
        {'name': u'курс2', 'cost': 25, 'lnk': '/krs2' },
        {'name': u'курс3', 'cost': 15, 'lnk': '/krs3' },
        {'name': u'курс4', 'cost': 13, 'lnk': '/krs4' },
        ]
        return render_to_response('ecpd/lk_call.html',
                                    {'top_items': top_items, 'items': items, 'ku_items': ku_items},
                                    context_instance=RequestContext(request))

def open_call(request):
        return render_to_response('ecpd/lk_ok.html',
                                    {'top_items': top_items, 'items': items, 'ku_items': ku_items},
                                    context_instance=RequestContext(request))
