"""es URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
#from performer import views as perf_views
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^lk/$', views.lk, name='lk'),
    url(r'^lk/(\w+)$', views.lk, name='lk'),

    url(r'^call$', views.call),
    url(r'^open_call$', views.open_call),
    url(r'^issue$', views.issue, name='lk'),
    url(r'^reg$', views.reg),
    url(r'^r_f$', views.r_f),
    url(r'^reg_usr$', views.reg_usr, name='lk'),
    url(r'^job$', views.job, name='lk'),

    url(r'^uslugi$', views.uslugi, name='lk'),
    url(r'^fb$', views.fb, name='lk'),
    url(r'^publ$', views.publ, name='lk'),
    url(r'^webinary$', views.webinary, name='lk'),
    url(r'^info$', views.info, name='lk'),

    url(r'^t_job$', views.t_job),
    url(r'^t_usl$', views.t_usl),
    url(r'^t_webinary$', views.t_webinary),
    url(r'^t_forum$', views.t_forum),
    url(r'^t_isp$', views.t_isp),

    #url(r'^accounts/$', include('registration.urls')),
    
    url(r'^cust/', include('customer.urls'), name='cust'),
    url(r'^perf/', include('performer.urls'), name='perf'),
    url(r'^admin/', admin.site.urls),
]
