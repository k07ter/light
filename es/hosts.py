from django.conf import settings
from django_hosts import patterns, host

urlpatterns = patterns('',
    #host(r'www', settings.ROOT_URLCONF, name='www'),
    #host(r'(\w+)', 'ecpd.urls', name='wildcard'),
    host(r'cust', 'customer.urls', name='cust'),
    #host(r'perf', 'performer.urls', name='perf'),
)
