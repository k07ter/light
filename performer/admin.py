#coding: utf-8

from django.contrib import admin
from performer.models import Performer

# Register your models here.
class PerformerAdmin(admin.ModelAdmin):
	def __str__(self):
		return u'%s' % self.name

	class Meta:
		model = Performer
		verbose_name = u'Подрядчик'
		verbose_name_plural = u'Подрядчики'

admin.site.register(Performer, PerformerAdmin)
