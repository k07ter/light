from django.conf.urls import url
from django.contrib import admin
from performer.views import *

urlpatterns = [
    url(r'^$', home),
    url(r'^(\d+)$', perf),
    url(r'^add$', new_acc),
]
