#coding: utf-8

from django.shortcuts import render, render_to_response
from performer.models import Performer
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from django.template import Template, RequestContext
#from django.core.context_processors import csrf

# Create your views here.
def home(request):
	all_perf = [{'name': u'Мила'}, {'name': u'Лена'}]
	perf_db = Performer.objects.all()
	print(perf_db)
	return render_to_response('performer/index.html', {'all_perf': all_perf})

def perf(request, arg):
	print(arg)
	all_perf = [{'name': u'Аня', 'nom': 2}, {'name': u'Маня', 'nom':3}]
	perf_db = Performer.objects.filter(att_nom=arg)
	print(perf_db)
	return render_to_response('performer/index.html', {'all_perf': all_perf, 'profile': arg})

#@csrf_protect
def new_acc(request):
	#print(request.method)
	if request.method == 'POST':
	    post = request.POST
	    #print(post)
	    fam = post.get('fam') or 'n'
	    name = post.get('imya') or 'f'
	    otch = post.get('otch') or 'ot'
	    town = post.get('town')
	    phone = post.get('phone')
	    email = post.get('email')
	    login = post.get('login')
	    parol = post.get('passw')
	    parol2 = post.get('passw_re')
	
	    #find_res = None
	    try:
	        #print(login)
	        find_res = Performer.objects.get(login=login)
	        #print(find_res.get('login'))
	        #usernm = authenticate(login, parol)
	        #if usernm is not None:
	        #    login(usernm)
	        #return render(u'уже есть')
	        #return HttpResponseRedirect('/')
	        if find_res is not None:
	            #print(find_res.email)
	            return render_to_response('performer/add_res.html', {'res': u'Ошибка. Аккаунт уже существует.', 'lnk': '/r_f', 'lnk_text': u'Повторить' }) #, context_instance=RequestContext(request))
	    except:
	        if parol == parol2:
	            new_acc = Performer.objects.create(name=name, fam=fam, otch=otch, user_status='w', payment_period_type='m', payment_status='1',perf_status='z',profkind_id='1', login=login, parol=parol, email=email)
	            #new_acc = Performer.objects.get(login=login)
	            print(new_acc)
	            #Performer.objects.create()
	            return render_to_response('performer/add_res.html', {'res': u'Аккаунт %s создан.' % login, 'lnk': '/lk', 'lnk_text': u'Войти' }) #, context_instance=RequestContext(request))
	            #return HttpResponseRedirect('ecpd/lk_cust.html', {'account': new_acc, 'data': post }, context_instance=RequestContext(request))
	            #return HttpResponseRedirect('/lk', {'account': new_acc }) #, context_instance=RequestContext(request))
	            #document.location.href = '/lk'
	        else:
	            return render_to_response('performer/add_res.html', {'res': u'Ошибка. Пароли не совпадают', 'lnk': '/r_f', 'lnk_text': u'Повторить' }) #, context_instance=RequestContext(request))
