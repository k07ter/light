#coding: utf-8
from django.db import models
from es.models import Person, Sprkind

PERSON_STATUS = (
	    ('W', u'Ожидает'),
	    ('D', u'Удален'),
	    ('B', u'В черном списке'),
	)

# Create your models here.
class Performer(Person):

        #rate = models.DecimalField(u'Рейтинг', max_digits=5, decimal_places=2)
        #fam = models.CharField(u'Фамилия', max_length=25)
        #name = models.CharField(u'Имя', max_length=15)
        #otch = models.CharField(u'Отчество', max_length=30)
        #passwd = models.CharFiled(u'Пароль', max_length=20)
        profkind = models.ForeignKey(Sprkind, verbose_name=u'Сфера деятельности')
        #issue = models.ForeignKey('Issue', verbose_name=u'Заявки')
        #issue_closed = models.ForeignKey(u'Выполненные сделки', 'Issue')
        #issue_sum = models.ForeignKey(u'Выполненные сделки', 'Issue')
        #payment_period_type = models.CharField(u'Период оплаты', choices=(('1','+'),('0','-')), max_length=1)
        #payment_status = models.CharField(u'Статус оплаты', choices=(('1','+'),('0','-')), max_length=1)
        perf_status = models.CharField(u'Статус исполнителя', choices=PERSON_STATUS, max_length=1)
        #documenty = models.File(u'Документы')

        class Meta:
               db_table = 'performers'
               verbose_name = u'Подрядчик'
               verbose_name_plural = u'Подрядчики'
        
        def __str__(self):
            return u'%s' % self.name