from django.conf.urls import include, url
from django.contrib import admin
#import es.customer
from .views import *

urlpatterns = [
    # Examples:
    url(r'^$', home, name='cust'),
    url(r'^(\d+)$', cust, name='cust'),
]
