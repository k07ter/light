from django.db import models
from es.models import Person, Sprorg
from datetime import datetime
from es import settings

# Create your models here.
class Secure(models.Model):
	key = models.CharField(u'Ключ', max_length=200)
	cust = models.ForeignKey('Customer', verbose_name=u'Заказчик')
	perf = models.ForeignKey('Performer', verbose_name=u'Исполнитель')

	class Meta:
	    abstract = True

class Customer(Person):
	#org = models.ForeignKey(Sprorg, max_length=7)
	cust = models.CharField(u'Код', max_length=15, default='000')
	#fullname = models.CharField(u'Полное наименование', max_length=255)
	#shortname = models.CharField(u'Краткое наименование', max_length=255)
	#adress_ur = models.CharField(u'Юридический адрес', max_length=255)
	#phone_ur = models.CharField(u'Телефон организации:', max_length=10)
	#email_ur = models.EmailField(u'Электронная почта:')
	fam_ruk = models.CharField(u'Фамилия:', max_length=25)
	name_ruk = models.CharField(u'Имя:', max_length=15)
	otch_ruk = models.CharField(u'Отчество:', max_length=35)
	#fam_usr = models.CharField(u'Фамилия:', max_length=25)
	#name_usr = models.CharField(u'Имя:', max_length=15)
	#otch_usr = models.CharField(u'Отчество:', max_length=35)
	doljnost = models.CharField(u'Должность:', max_length=35)
	#phone = models.CharField(u'Телефон основного пользователя:', max_length=10)
	#email = models.EmailField(u'Электронная почта основного пользователя:')
	#other_cont = models.CharField(u'Другие контакты:', max_length=100)
	#ogrn = models.CharField(u'ОГРН:', max_length=15)
	#inn = models.CharField(u'ИНН:', max_length=14)
	#ur_type = models.CharField(u'Сфера деятельности:', choices=(('1','+'),('0','-')), max_length=3)
	date_reg = models.DateField(u'Дата регистрации на портале:', default=datetime.now)
	#login = models.CharField(u'Логин:', max_length=20, default='login')
	#passwd = models.CharField(u'Пароль', max_length=20, default='1234')
	#passwd = models.ForeignKey(Secure, verbose_name=u'Пароль', max_length=20)
	#service_type = models.ForeignKey(u'Тип услуг', 'Service')
	#payment_period_type = models.CharField(u'Период оплаты', choices=PAYMENT_PERIOD, default='w', max_length=3)
	#payment_status = models.IntegerField(u'Статус оплаты', choices=PAYMENT_STATUS, default=0)
	documenty = models.FileField(u'Документы', upload_to=settings.UPLOADS_ROOT + 'docs/', blank=True)

	class Meta:
		db_table = 'customers'
		verbose_name = u'Заказчик'
		verbose_name_plural = u'Заказчики'

	def __unicode__(self):
		return u'%s' % self.shortname
