#coding: utf-8

from django.contrib import admin
from customer.models import Customer

# Register your models here.
class CustomerAdmin(admin.ModelAdmin):
	def __unicode__(self):
		return u'%s' % self.shortname

	class Meta:
		model = Customer
		verbose_name = u'Заказчик'
		verbose_name_plural = u'Заказчики'

admin.site.register(Customer, CustomerAdmin)
